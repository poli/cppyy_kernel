import io
from typing import ContextManager

def sys_pipes() -> ContextManager[tuple[io.StringIO, io.StringIO]]: ...
