from typing import Any
from IPython.display import HTML


class Magic:
    kernel: "MetaKernel"
    code: str


class MetaKernel:
    def do_execute(self, code: str) -> None: ...
    def do_execute_direct(self, code: str, **kw: Any) -> None: ...
    def raw_input(self, prompt: str) -> str: ...
    def register_magics(self, m: type[Magic]) -> None: ...
    def Print(self, arg: str, **kw: Any) -> None: ...
    def Error(self, arg: str, **kw: Any) -> None: ...
    def Display(self, arg: HTML, **kw: Any) -> None: ...
    
