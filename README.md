[TOC]

## Goal of this project

Experimentation and Development Department (SED) at Inria Saclay regularly proposes [C++ training session](https://gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp) based on a lecture using Jupyter notebooks.

To run C++ code in notebooks, we were relying upon [Xeus-cling](https://github.com/jupyter-xeus/xeus-cling), which itself uses [cling](https://github.com/root-project/cling) interpreter.

However, Xeus-cling project doesn't seem to evolve lately, and we were stuck by:

- [Lack of C++ 20 support](https://github.com/jupyter-xeus/xeus-cling/issues/510), which was problematic as we intend to follow closely C++ standards widely supported by clang and gcc compilers.
- [Lack of macOS](https://github.com/jupyter-xeus/xeus-cling/issues/403) since 2021, with no resolution in sight (less problematic than the C++20 hurdle as we can circumvent with Docker).

We looked a bit for alternatives and didn't find much, so we decided to define a very lightweight kernel leveraging [cppyy](https://github.com/wlav/cppyy), which provides Python bindings on top of cling.

To sum up, our primary goal with the current project is to provide usable notebooks for our C++ lecture.

By contacting authors of `cppyy` we have learnt since of another active project to run C++ code in notebooks:

- [xeus-cpp](https://xeus-cpp.readthedocs.io/en/latest/): a successor to xeus-cling, based on [clang-repl](https://clang.llvm.org/docs/ClangRepl.html)



## Installation

### Binder: the no installation option

If you just want to give it a try, you may just use the Binder link provided by a badge on the [Gitlab project](https://gitlab.inria.fr/sed-saclay/cppyy_kernel); Binder will deploy a working environment in your browser to run a notebook.

It is by far the easiest way - just one click! - but unfortunately:

- The construction of the image may take some time
- Binder is not completely reliable and the construction of the image may fail, in which case you just have to try again and cross your fingers...


### Local installation

Installation was checked on Ubuntu and macOS systems.

Requirements: python v3.12

* Install jupyterlab (typically through conda, mamba or pip).
* Install the latest version from the repository

```shell
pip install git+https://gitlab.inria.fr/sed-saclay/cppyy_kernel.git
```

(alternatively you may target a specific tag such as v0.1.0 with:

```shell
pip install git+https://gitlab.inria.fr/sed-saclay/cppyy_kernel.git@v0.1.0
```
)

If you want to limit the version of the C++ standard to use, please [have a look here](https://cppyy.readthedocs.io/en/latest/installation.html#c-standard-with-pip).

### Developer version

If you intend to tweak the kernel, you should consider the following commands instead:

```shell
git clone https://gitlab.inria.fr/sed-saclay/cppyy_kernel
pip install -e ./cppyy_kernel
```
By doing so, updates made in your local copy of the code are immediately taken into account after a restart of the kernel for the notebook you're working on.


## Running the Docker image

Alternatively if you do not want to bother with installing CppyyKernel in your environment, you may consider using the Docker image; to do so you just need to install [Docker](https://www.docker.com/) on your computer.

At each release, a Docker image with a working Cppyy Kernel is also released on the [container registry](https://gitlab.inria.fr/sed-saclay/cppyy_kernel/container_registry) of the project; the Dockerfile used to generate it may be found in *docker* directory.

**WARNING:** The image was generated in Inria CI with a Linux 64 environment. Despite a warning if you're running it from an ARM environment (typically a Mac M1, M2, etc...) it should run mostly fine... except for some very weird edge cases (one of which is documented in `notebooks/ARM_docker.ipynb`). If possible under macOS you should therefore use a local installation instead. A Docker image generated under macOS works just fine, but none is generated so far in our CI pipeline.

To use it:

First get the image from Gitlab registry:

```shell
docker login registry.gitlab.inria.fr
docker pull registry.gitlab.inria.fr/sed-saclay/cppyy_kernel:latest
```

Then run a container with:

```shell
docker run --rm -e JUPYTER_TOKEN='easy' -p 8888:8888  --cap-drop=all registry.gitlab.inria.fr/sed-saclay/cppyy_kernel:latest
```

And in your browser type `http://localhost:8888`

and then type `easy` in the token dialog box (of course you may replace by whatever you want).

Few hints for those not familiar with Docker:

* `--cap-drop=all` is a safety when you're running a Docker image not built by yourself: you're essentially blocking the few remaining operations that might impact your own environment that Docker lets by default open with the run command.
* `-p` gives the port mapping between the Docker image and your local environment.
* `--rm` tells docker to delete the container after its use.

The lengthy `registry.gitlab.inria.fr/sed-saclay/cppyy_kernel:latest` is the name of the Docker **image**, if this image is not present locally in your environment Docker will try to fetch it from a *registry* on the Inria Gitlab.


## Usage

To use this kernel, install `jupyter lab` then run one of them:

```shell
jupyter lab
```

If the kernel starts properly, you should be able to run C++ code in the cells.

The command

```shell
jupyter kernelspec list
```

should show Cppyy in the list of known kernels.

You may check it works properly by loading any notebook from the _notebooks_ directory.



### Magics

This kernel is based on [MetaKernel](http://pypi.python.org/pypi/metakernel), which means it features a standard set of magics (such as `%%file myfile.txt` to write the content of the current cell into *myfile.txt'). For a full list of magics, you may run `%lsmagic` in a cell.

We developed several magics for our C++ needs; you may obtain the full list and an explanation of their purpose by running in a cell:

```
%%cppmagics
;
```

(the cell must not be empty; semicolon may be replaced by another instruction).

Please reach to us if the help provided for a given type is not clear enough!

Most of these new magics are there to handle specific cases that are not directly supported by the kernel:

- If you intend to play with standard input (`std::cin`), please use the related magics that plug notebook dialog boxes to standard input. This is really a best effort - no guarantee it will cover all your needs! (it works well enough for our [dedicated notebook](https://gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/-/blob/master/1-ProceduralProgramming/6-Streams.ipynb) in C++ tutorial).

- Sometimes we want to interpret directly the content of a cell as a program to be compiled and run. We introduced magics `clang`, `gcc` and `anycppcompiler` to cover these.

- `%%cppmagics cppyy/cppdef`: cppyy in fact defines two different functions `cppexec` and `cppdef`. The former is for code deemed to be executed, the latter for code that defines classes, functions and so on. In our Jupyter kernel we use by default `cppexec`, which works just fine for most operations. However some really need to call under the hood `cppdef`; in this case you need to use this magics. It's the case for instance for (you may have a look to *notebooks/Cppdef.ipynb* to see simple examples of these):
    * Forward declarations.
    * Structure bindings (C++ 17).
    * Friendship to a template class.



## Know limitations

### Asserts

The `cling` that is used by `cppyy` was compiled with some optimizations enabled; in this case `NDEBUG` macro is defined and the `assert()` command does absolutely nothing.

We circumvented it by a hack:

- We define ourselves the `assert()` macro, which writes the assert message on `std::cerr` and then throws an exception to interrupt the code running from the cell.
- We remove the includes `cassert` or `assert.h` in the block of code.

Doing so mimics the behaviour of a true assert; we edited the message to make it clear it is not truly C++ assert that is involved (remember: our point is to be able to underline in our training session what `assert` does!)

It is not entirely fullproof: if you include others STL includes that themselves include `cassert` or `assert.h`, `assert()` call will revert to its usual behaviour in release mode - do nothing.

### Exceptions

Currently an uncaught exception doesn't provide an adequate error message.

We are in discussion with authors of `cppyy`, as its behaviour is a bit weird: there are two indications that are both incorrect (one tells compilation failed, and the other that the code couldn't be parsed properly).


## Contributing

If you are willing to contribute to this code, feel free to propose a merge request!

If you can't (for instance due to a lack of Gitlab account) please contact us at sed-saclay [AT] inria.fr.
