from metakernel import Magic
from argparse import ArgumentParser
import shutil
from .util import CLANG, GCC, CPP_STANDARDS


class CppMagics(Magic):
    def cell_cppmagics(self, line: str) -> None:
        """
        %%cppmagics TOOLNAME - execute the cell code using the named tool

        Example:
            %%cppmagics clang
            Compile then execute the cellcode using clang compiler
        """
        # unfortunately magic.option doesn't seem to work well
        # when the user command contains options too.
        # That's why one use argparse here
        line = line.strip()
        parser = ArgumentParser(description="Process %%cppmagics own options.")
        parser.add_argument(
            "-p", "--print-command", action="store_true", dest="print_command"
        )
        cmd_list = line.split(" ")
        len_ = 0
        for i, token in enumerate(cmd_list):
            len_ += len(token)
            if not token.startswith("-"):
                break
        args, unknown = parser.parse_known_args(cmd_list[:i])
        if unknown:
            self.kernel.PrintKernelError(  # type: ignore
                f"Unrecognized option(s): {' '.join(unknown)}. Known options are: \n\t--{"\n\t--".join(vars(args).keys())}"
            )
        else:
            self.kernel.do_execute_direct(
                self.code,
                magics={
                    "toolname": token,
                    "toolargs": line[len_ + i:].strip(),
                    "print_command": args.print_command,
                },
            )
        self.evaluate = False

    def help_line(self) -> None:
        self.kernel.Print("Authorized commands are:")
        self.kernel.Print("  ls-compilers - list the standard used for each compiler")
        self.kernel.Print("  set-clang-std - set the standard for clang++ compiler")
        self.kernel.Print("  set-gcc-std - set the standard for g++ compiler")
        self.kernel.Print("  help - display this message!")

    def line_cppmagics(self, line: str) -> None:
        """
        %cppmagics ACTION - execute the ACTION (the cell body is ignored)

        Example:
            %cppmagics ls-compilers
            List the c++ standards supported by the compilers
        """
        line = line.strip()
        if line == "ls-compilers":
            for compiler in (CLANG, GCC):
                std = self.kernel.get_std(compiler)  # type: ignore
                if not shutil.which(compiler):
                    self.kernel.Print(f"{compiler} was not found!")
                elif std is None:
                    self.kernel.Print(f"The {compiler} is probably too old")
                else:
                    self.kernel.Print(f"The C++ standard for {compiler} is {std}")
        elif line.startswith("set-clang-std ") or line.startswith("set-gcc-std "):
            compiler = CLANG if line.startswith("set-clang-std ") else GCC
            _, std = line.split(" ", 1)
            if std not in CPP_STANDARDS[compiler]:
                if not self.kernel._check_compiler_std(compiler, std):  # type: ignore
                    self.kernel.PrintKernelWarning(  # type: ignore
                        f"{std} is not in {CPP_STANDARDS[compiler]}. Hope you know what you're doing ..."
                    )
            self.kernel.set_std(compiler, std)  # type: ignore
        elif line == "help":
            self.help_line()
        else:
            self.kernel.PrintKernelError(f"Unknown action {line}")  # type: ignore
            self.help_line()
