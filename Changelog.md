# v0.3.0

- #41, #43: A mechanism to provide flags to magics has been added and used to provide an optional `-p` flag
telling whether the terminal command should be printed in the outputs or not.



# v0.2.0

First stable release of CppyyKernel, which provide solutions to many issues
we had identified when the v0.1.0 was published.

Our first goal is met: our [C++ training](https://gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp)
may now be run entirely with the kernel (this is not yet the case for its main branch as the MR that will
provide the update was stalled until the present tag is published)


- #28, #25, #40, #36, #32, #33: Introduce magics to be able to run content of a given cell directly with a command line tool (be it
a compiler such as `gcc` or `clang`, a tool such as `clang-tidy`). The cell must provide all the context. This
functionality enables to limit drastically the use of online compilers in our C++ training.
- #3: Magics are now provided to enable `std::cin` within a notebook (a dialog box is opened).

- #5, #6, #9, #16, #27: Improve the management of warnings and error messages.

- #8, #34: Provide a hacky way to mimic `assert` behaviour. It is explicitly underlined when used that it is a
hack that might not work in all configurations.

- #7, #10, #13: Introduce a magic `cppyy/cppdef` to use explicitly under the hood `cppyy.cppdef` instead of the
`cppyy.cppexec` we use by default for conveniency. Some C++ operations require the finer `cppdef` (the
cases we have identified are underlined in `notebooks/Cppdef.ipynb` notebook.

- #14, #15, #20, #22, #26, #38, #39: improve tests, CI, deployment, Docker and various dev tooling of the project.



# v0.1.0

First published version, during a sprint made by SED Saclay on the 23rd and 24th of May 2024.

This version was mostly a proof of concept, with many cells already working but some issues identified.