All contributions are welcome; please issue a merge request on [main project](https://gitlab.inria.fr/sed-saclay/cppyy_kernel).

If you intend to contribute, please install pre-commit facility which prevents committing notebooks with some cells already executed:

```shell
pip install pre-commit
pre-commit install
```